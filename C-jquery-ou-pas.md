<img src="images/readme/header-small.jpg" >

# C. jQuery or not jQuery ?<!-- omit in toc -->

_**Maintenant que l'on connaît les fonctions de base de jQuery, voyons ce que ça donne une fois tout le code de l'application converti.**_

## Sommaire <!-- omit in toc -->
- [C.1. Migrer à jQuery](#c1-migrer-à-jquery)
- [C.2. Recoder jQuery ?](#c2-recoder-jquery-)
- [C.3. Conclusion](#c3-conclusion)

## C.1. Migrer à jQuery
**Modifiez le code de l'application pour utiliser jQuery au lieu des méthodes classiques de l'API DOM (`querySelector`, `innerHTML`, `classList.add/remove`, `addEventListener`, ...).**

> _**Astuce :** Quand vous stockez un objet jQuery dans une variable, une convention de nommage courramment employée consiste à **préfixer le nom de la variable avec un `$`** (pour indiquer qu'il s'agit d'un objet jQuery à l'intérieur)._ \
> _Par exemple :_
> ```js
> const $mainMenu = $('.mainMenu');
> ```

## C.2. Recoder jQuery ?

1. Maintenant que toute votre application a été "convertie" à jQuery, pour comprendre pourquoi certains veulent se passer de jQuery, je vous invite à comparer le poids du bundle de votre application (`build/app.bundle.js`) avec le poids de celui du TP précédent. \
	**Que constatez vous ?**
2. **Recompilez les deux TPs en mode "production"** (`npm run build`). Constatez vous la même chose ?
4. Si le coeur vous en dit et s'il vous reste de l'énergie, **je vous propose de désinstaller `jQuery` (`npm uninstall jquery`) et de coder votre propre fonction `$()`**. Créez-vous un module à part (par exemple dans `src/lib/jqlite.js`) et codez y la fonction `$()` (_n'oubliez pas de l'exporter !_).

	Cette fonction `$()` devra fonctionner **exactement** comme celle de jQuery, c'est à dire que vous ne devez pas avoir à toucher au reste du code de votre appli (_sauf pour changer les `import`_).

	Vous pouvez vous aider de http://youmightnotneedjquery.com/ mais gardez à l'esprit que leurs exemples ne gèrent pas le fait qu'un sélecteur peut correspondre à plusieurs balises et qu'il faut parfois "boucler" sur plusieurs éléments !

	Une fois tout ça terminé, comparez le poids du bundle ainsi généré avec celui de jquery.

## C.3. Conclusion

**Si vous avez terminé ce TP vous pouvez être fiers de vous 👏 !**\
**Gardez quand même à l'esprit que jQuery gère tout un tas de choses que vous n'avez pas codées :**
- une grande quantité de fonctionnalité qu'on a pas utilisé dans pizzaland : les animations, les méthodes show/hide, ajax, etc., etc.
- une multitude de cas limites que vous ne gérez pas dans votre code
- et surtout qu'elle garanti que son code fonctionne sur [énormément de navigateurs](https://jquery.com/browser-support/) (ce qui n'est probablement pas le cas de notre lib "maison" :wink: ).
